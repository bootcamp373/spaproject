import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddreviewComponent } from './addreview/addreview.component';
import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';
import { ServicesComponent } from './services/services.component';

const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "home", component: HomeComponent },
  { path: "search", component: SearchComponent },
  { path: "services", component: ServicesComponent },
  { path: "addreview", component: AddreviewComponent },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
