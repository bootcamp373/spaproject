import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Service } from '../models/service.model';

import { ServicesService } from '../providers/services.service';
import { CategoriesService } from '../providers/categories.service';
import { Category } from '../models/category.model';
@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent {
  
  services: Array<Service> = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private servicesService: ServicesService
  ) { }
  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params) => {
      let id = params['id'];
      this.servicesService.getServices().subscribe((data)=>{
        this.services = data.filter(d => d.ServiceID.startsWith(id.substring(0,4)) );  
     })
    });
  }
}
