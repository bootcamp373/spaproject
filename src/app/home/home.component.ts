import { Component, OnInit } from '@angular/core';
import { ReviewsComponent } from '../reviews/reviews.component';
import { Reviews } from '../models/reviews.model';
import { ReviewsService } from '../providers/reviews.service';

import { Category } from '../models/category.model';
import { CategoriesService } from '../providers/categories.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent {

  reviews: Array<Reviews> = [];

  constructor(
    private reviewsService: ReviewsService
  ) { }
  ngOnInit() {

    this.reviewsService.getReviews().subscribe((data) => {
    this.reviews = data;
    });

  }
}




















