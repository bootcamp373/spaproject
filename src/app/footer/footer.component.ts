import { Component, OnInit } from '@angular/core';
import { ReviewsComponent } from '../reviews/reviews.component';
import { Reviews } from '../models/reviews.model';
import { ReviewsService } from '../providers/reviews.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  reviews: Array<Reviews> = [];

  constructor(
    private reviewsService: ReviewsService
  ) { }
  ngOnInit() {

    this.reviewsService.getReviews().subscribe((data) => {
      this.reviews = data;
    });

  }
}
