import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Category } from './../models/category.model';
import { ImageLoader } from '@angular/common';
import { Reviews } from '../models/reviews.model';

@Injectable({
  providedIn: 'root'
})
export class ReviewsService {

  constructor(private http: HttpClient) { };

  // set up the endpoint and any HTTP headers
  private ReviewEnd: string =
    'http://localhost:8081/api/reviews';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': "*"

    })
  };


  getReviews(): Observable<Reviews[]> {
    return this.http.get(this.ReviewEnd,
      this.httpOptions)
      .pipe(map(res => <Reviews[]>res));
  }
  getReview(id: number): Observable<Reviews> {
    return this.http.get(this.ReviewEnd + "/" + id,
      this.httpOptions)
      .pipe(map(res => <Reviews>res));

  }
  postNewReview(reviews: Reviews): Observable<Reviews> {
    return this.http.post<Reviews>('http://localhost:8081/api/reviews/',
      {
        date: reviews.date,
        reviewer: reviews.reviewer,
        comment: reviews.comment
      },
      this.httpOptions);
  }

}
